#div1 {
	height: 150px;
	width: 400px;
	margin: 20px;
	border: 1px solid red;
	padding: 10px;
}
/*
To find the total Height of the element, we use the following equation: margin-top + border-top + padding-top + (height of the content) + padding-bottom + border-bottom + margin-bottom.
Total Height: 20px + 1px + 10px + 150px + 10px + 1px + 20px = 212px

To find the total Width of the element, we use the following equation: margin-left + border-left + padding-left + (width of the content) + padding-right + border-right + margin-right.
Total Width: 20px + 1px + 10px + 400px + 10px + 1px + 20px = 462px

The difference between finding Total Height/Width and Browser calculated Height/Width is that the Browser does not include the margin in it's calculation.

To find the Browser Calculated Height, we use the following equation: border-top + padding-top + (height of the content) + padding-bottom + border-bottom.
Browser Calculated Height: 1px + 10px + 150px + 10px + 1px = 172px

To find the Browser Calculated Width, we use the following equation: border-left + padding-left + (width of the content) + padding-right + border-right.
Browser Calculated Width: 1px + 10px + 400px + 10px + 1px = 422px
*/